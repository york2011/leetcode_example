/**
 * 
 * @param {number[][]} grid 
 * @returns {number}
 */
var minPathSum = function(grid) {
    // solution 1:
    // var m = grid.length || 0;
    // var n = (grid[0] || []).length || 0;
    // if ( m===0 ) { return 0 };
    // if ( m===1 && n===1 ) { return grid[0][0] };
    // if ( m===1 && n!==1 ) { return grid[0].reduce(function (a, b) {return a + b}, 0) };
    // if ( m!==1 && n===1 ) { return grid.reduce(function(a, b) {return a + b[0]}, 0) };
    // return grid[m-1][n-1] + Math.min(
    //     minPathSum(grid.slice(0, m-1)),
    //     minPathSum(grid.map(function(row) { return row.slice(0, row.length -1)}))
    // );
    // solution 2:
    var m = grid.length || 0;
    var n = (grid[0] || []).length || 0;

    if ( m===0 ) {return 0};
    for ( var i = 0 ; i < m ; i++) {
        for(var j = 0 ; j < n ; j++) {
            if ( i === 0 && j === 0) continue;
            if ( i === 0) grid[i][j] += grid[i][j-1];
            else if (j===0) grid[i][j] += grid[i-1][j];
            else grid[i][j] += Math.min(
                grid[i][j-1],
                grid[i-1][j]
            );
        }
    }
    return grid[m-1][n-1];
};


/**
 * 
 * https://leetcode.com/submissions/detail/641341291/
 */

console.log(minPathSum( [ [1,3,1],[1,5,1],[4,2,1] ] ));

