/**
 * 
 * @param {number[]} nums
 * @returns {number}
 */
var maxSubArray = function(nums) {

    var maxSum = nums[0];
    var sum = nums[0];

    for (var i = 1 ; i < nums.length; i++) {
        // 目前累加的總和是正的，繼續加，反之重新來過
        if ( sum > 0 ) { 
            sum += nums[i];
        } else {
            sum = nums[i];
        }
        // 如果累加的總和超過歷史加總最大值時，更新最大值
        if (sum > maxSum) {
            maxSum = sum;
        }
    }
    return maxSum;
}
/**
 * https://leetcode.com/submissions/detail/641329002/
 * 
 */

var result = maxSubArray([-2,1,-3,4,-1,2,1,-5,4]);
console.log(result);
